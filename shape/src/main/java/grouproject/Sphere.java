// Patricia Boujeily
package grouproject;

/**
* Sphere is a class used to create a sphere 
* information about a Sphere
* @author Patricia Bourjeily
* @version 10/4/2023
*/
public class Sphere implements Shape3d {
    private double radius;

    /**
    * Constructor for the field radius
    *
    * @param radius The value to get
    */
    public Sphere(double radius){
        if (this.radius > 0){
            this.radius = radius;
        }
        else
        {
            throw new IllegalArgumentException("The radius must not be negative");
        }
    }

    /**
    * Gets the radius of the sphere
    *
    * @return The value of this.radius 
    */
    public double getRadius(){
        return this.radius;
    }

    /**
    * Doubles the input value
    *
    * @param radius The value to be set
    */
    public void setRadius(double radius){
        this.radius = radius;
    }

    /**
    * Gets the volume of the sphere
    *
    * @return The value of the volume of the sphere
    */
    public double getVolume(){
        return (4.0/3 * Math.PI * Math.pow(this.radius, 3));
    }

    /**
    * gets the surfaceArea of the sphere
    *
    * @return The value of the volume of the sphere
    */
    public double getSurfaceArea(){
        double surface = 4 * Math.PI * Math.pow(this.radius, 2);
        return surface;
    }
}
