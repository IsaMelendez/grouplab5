package grouproject;

/**
 * Cone is a class that implements the Shape3d interface and stores information about a cone
 * @author Isa Melendez
 * @version 10/4/2023
 */
public class Cone implements Shape3d{
    private double height;
    private double radius;

    public Cone(double height, double radius){
        if(height > 0 && radius > 0){
            this.height = height;
            this.radius = radius;
        }
        else{
            throw new IllegalArgumentException("Values must be bigger than 0!");
        }
    }

    /**
     * Method that calculates the surface area of the cone.
     * @return Returns a double representing the surface area.
     */
    public double getSurfaceArea(){
        return Math.PI*this.radius*(this.radius+Math.sqrt(Math.pow(this.height, 2)+Math.pow(this.radius, 2)));
    }

    /**
     * Method that calculates the volume of the cone.
     * @return Returns a double representing the volume.
     */
    public double getVolume(){
        return Math.PI*Math.pow(this.radius, 2)*(this.height/3);
    }

    /**
     * Method that returns the radius of the cone's base
     * @return Returns a double representing the radius
     */
    public double getRadius(){
        return this.radius;
    }

    /**
     * Method that calculates the height of the cone.
     * @return Returns a double representing the height.
     */
    public double getHeight(){
        return this.height;
    }
    
    /**
     * Method that changes the value of the radius.
     * @param radius The new value for the radius.
     */
    public void setRadius(double radius){
        this.radius = radius;
    }

    /**
     * Method that changes the value of the height.
     * @param radius The new value for the height.
     */
    public void setHeight(double height){
        this.height = height;
    }
}
