package grouproject;

import static org.junit.Assert.*;

import org.junit.Test;

/**
* SphereTesting is a class used for testing the Sphere class' methods
* @author Isa Melendez
* @version 10/11/2023
*/
public class SphereTesting 
{
    /**
     * The following test will attempt to create a sphere with a negative radius. If the constructor is throwing an exception, the method will succeed.
     */
    @Test
    public void testCreateSphereNegative()
    {
        try{
            Sphere s = new Sphere(-5);
            fail("An IllegalArgumentException should've been thrown.");
        }
        catch(IllegalArgumentException e){}
    }

    /**
     * The following test will attempt to create a sphere with a radius equal to 0. If the constructor is throwing an exception, the method will succeed.
     */
    @Test
    public void testCreateSphereZero()
    {
        try{
            Sphere s = new Sphere(0);
            fail("An IllegalArgumentException should've been thrown.");
        }
        catch(IllegalArgumentException e){}
    }

    /**
     * The following test will check that the volume of a sphere is calculated correctly.
     */
    @Test
    public void testSphereVolume()
    {
        Sphere s = new Sphere(5);
        assertEquals(523.59878, s.getVolume(), 0.0001);
    }

    /**
     * The following test will check that the surface area of a sphere is calculated correctly.
     */
    @Test
    public void testSphereSurfaceArea()
    {
        Sphere s = new Sphere(5);
        assertEquals(314.15927, s.getSurfaceArea(), 0.0001);
    }

    /**
     * The following test will test the getRadius method
     */
    @Test
    public void testGetRadius()
    {
        Sphere s = new Sphere(5);
        assertEquals(5, s.getRadius(), 0.0001);
    }

    /**
     * The following test will test the setRadius method
     */
    @Test
    public void testSetRadius()
    {
        Sphere s = new Sphere(5);
        s.setRadius(6);
        assertEquals(6, s.getRadius(), 0.0001);
    }
}
