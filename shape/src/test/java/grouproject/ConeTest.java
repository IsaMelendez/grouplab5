package grouproject;

import static org.junit.Assert.*;

import org.junit.Test;

/**
* ShapeTest is a class used for testing packages out
* information about a Cone
* @author Patricia Bourjeily
* @version 10/11/2023
*/
public class ConeTest 
{

    /**
     * Method to test the getter Radius
     * if value is 0
     */
    @Test
    public void testGetRadius()
    {
        Cone firstCone = new Cone(3, 2);
        assertEquals(2, firstCone.getRadius(), 0.01);
    }


    /**
     * Method to test the getter Height
     * if value is correct
     */
    @Test
    public void testGetHeight()
    {
        Cone firstCone = new Cone(5, 2);
        assertEquals(2, firstCone.getRadius(), 0.01);
    }


    /**
     * Method to test the setter Radius
     * if value is correct
     */
    @Test
    public void testSetRadius()
    {
        Cone firstCone = new Cone(6, 2);
        firstCone.setRadius(3);
        assertEquals(firstCone.getRadius(), 3, 0.01);
    }


    
    /**
     * Method to check the setter Height
     * if value is correct
     */
    @Test
    public void testSetHeight()
    {
        Cone firstCone = new Cone(1, 2);
        firstCone.setRadius(1);
        assertEquals(firstCone.getRadius(), 1, 0.01);
    }

    
    /**
     * Method to check the constructor Height
     * if value is negative
     */
    @Test
    public void testConstructorNegHeight()
    {
        try {
            Cone firstCone = new Cone(-4, 6);
            fail("The test did not pass, because the exception was not throwd");
        }
        catch(IllegalArgumentException e){

        }
    }

    
    /**
     * Method to check the getter Radius
     * if value is negative
     */
    @Test
    public void testConstructorNegRadius()
    {
        try {
            Cone firstCone = new Cone(4, -6);
            fail("The test did not pass, because the exception was not throwd");
        }
        catch(IllegalArgumentException e){

        }
    }


    /**
     * Method to check the method GetSurfaceArea
     * if value is correct
     */
    @Test
    public void testGetSurfaceArea()
    {
        Cone firstCone = new Cone(1,2);
        assertEquals(26.62, firstCone.getSurfaceArea(), 0.01);
    }


    /**
     * Method to check the method getVolume
     * if value is correct
     */
    public void testgetVolume()
    {
        Cone firstCone = new Cone(5,6);
        assertEquals(157.07963, firstCone.getVolume(), 0.01);
    }

}
